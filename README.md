# Freecontest forward cms messages

Project setup

`npm install`

Run the project

`npm run start`

**On client-side**

To answer a question: follow the pattern

`#<question_id>:<answer>`

To manually index the starting question, change the current property in *current.json*