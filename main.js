const fb = require('facebook-chat-api')
require('dotenv').config({ silent: true });
const request = require('request')
const cheerio = require('cheerio')
const fs = require('fs')
const path = require('path')

//Lib

function RequestSync(isGet, opts) {
    // console.log('requesting...')
    return new Promise(function(resolve, reject) {
        if (isGet) {
            request.get(opts, function (err, httpResponse, body) {
            if (err) return reject({ err, httpResponse })
            resolve({ httpResponse, body })
            })   
        }
        else {
            request.post(opts, function (err, httpResponse, body) {
            if (err) return reject({ err, httpResponse })
            resolve({ httpResponse, body })
            })
        }
    })
}

function GetSync(opts) {
    return RequestSync(1, opts)
}

function PostSync(opts) {
    return RequestSync(0, opts)
}

//Cookie

function parseCookie(c) {
    let raw = c.split(';').map((pair) => pair.trim())
    let cookie = []
    for (let pair of raw) {
        let epos = pair.indexOf('=')
        pair = [ pair.substring(0, epos), pair.substring(epos + 1, pair.length) ]
        cookie[pair[0]] = pair[1]
    }
    return cookie
}

//XSRF

async function _XSRF(opts = {}) {
    const { httpResponse, body } = await GetSync(opts)
    let cookie = httpResponse.headers['set-cookie'].join('; ')
    return {
        cookie: cookie,
        _xsrf: _xsrfFromCookie(cookie)
    }
}

    function _xsrfFromCookie(c) {
    return parseCookie(c)._xsrf
}
  

//Login

async function Login() {
    // console.log('loging in...')
    let endpoint = process.env.AWS_URL + '/login'
    let opts = { url: endpoint }
    let { cookie, _xsrf } = await _XSRF(opts)

    opts = { 
        ...opts,
        headers: { 'cookie': cookie },
        formData: {
        _xsrf,
        username: process.env.user,
        password: process.env.password
        } 
    }

    const { httpResponse, body } = await PostSync(opts)
    cookie = httpResponse.headers['set-cookie'].join('; ')

    return cookie
}
  

//Main

async function getTasks(cookie) {
    let endpoint = process.env.AWS_URL + '/tasks'
    let opts = {
      url: endpoint,
      headers: { 'cookie': cookie }
    }
  
    let { httpResponse, body } = await GetSync(opts)
    let $ = cheerio.load(body)
  
    let tasks = []
    $('tr').each(function(_) {
      if (_ === 0) return ;
      let task = {}
      $(this).children('td').each(function(i) {
        if (i === 0) task.id = $(this).children('input[name="task_id"]').attr('value')
        if (i === 1) task.href = $(this).children('a').attr('href')
        if (i === 2) task.name = $(this).text().trim()
      })
      tasks.push(task)
    })
  
    return tasks.sort((a, b) => a.id - b.id)
  }

getQuestions = async (cookie) => {
    // console.log('getting Questions...')
    let endpoint = process.env.AWS_URL + '/contest/' + process.env.contestID + '/questions'
    let opts = {
        url: endpoint,
        headers: { 'cookie': cookie }
    }

    let { httpResponse, body } = await GetSync(opts)
    let $ = cheerio.load(body)  
    
    let questions = []

    $('div.notification.communication').each((_, e) => {
        e = $(e).children('div')[0]
        let subject = $(e).children('div.notification_subject').first().text(),
            question = $(e).children('div.notification_text').first().text()
            id = $(e).children('div.reply_question').first().children('form').first().attr('action').split('/')[5]
        // console.log({ sub: subject, ques: question })
        questions.push({ subject: subject, question: question, id: id })
    })

    // console.log(questions)
    return questions
}

replyQuestion = async (_cookie, qid, answer) => {
    // console.log('Replying...')
    
    let endpoint = process.env.AWS_URL + '/contest/' + process.env.contestID + '/question/' + qid + '/reply'
    let opts = { url: process.env.AWS_URL, headers: { 'cookie': _cookie } }
    let { cookie, _xsrf } = await _XSRF(opts) 
    opts = {
        url: endpoint,
        headers: { 'cookie': cookie },
        formData:{
            _xsrf,
            reply_question_quick_answer: 'other',
            reply_question_text: answer
        }
    }

    const { httpResponse, body } = await PostSync(opts)
}

let current = 0

const reply = async (qid, ans) => {
    let cookie = await Login()
    replyQuestion(cookie, qid, ans)
}

(async () => {
    let res = await fs.readFileSync(path.resolve(__dirname, './current.json'), 'utf-8')
    current = JSON.parse(res).current
    // console.log(current)

    fb({ email: process.env.email, password: process.env.pass }, async (err, api) => {
        if(err) return console.log(err)
        
        //Check for questions
        setInterval(async () => {
            // console.log('Current: ', current)
            let cookie = await Login()
            let res = await getQuestions(cookie)
            res = res.reverse()
            res = res.slice(current)
            if(res.length > 0){
                res.forEach(q => {
                    api.sendMessage('*#' + q.id + ': ' + q.subject + ' *\n ' + q.question, process.env.threadID)
                    current = q.id
                    console.log('Got new question (' + q.id + ')')
                })
                fs.writeFile(path.resolve(__dirname, './current.json'), '{ "current": ' + current + '}', (err) => {
                    if(err) console.log(err)
                })
            }
        }, 3000)

        //Check for replies
        api.listen((err, message) => {
            if(message.body.slice(0, 1) == '#' && message.body.indexOf(':') != -1){
                let qid = message.body.slice(1, message.body.indexOf(':'))
                    ans = message.body.slice(message.body.indexOf(':') + 1)
                
                console.log("Replied to question (" + qid + ")")
                reply(qid, ans)
            }
        })
    })
})()